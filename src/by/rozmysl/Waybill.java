package by.rozmysl;

import by.rozmysl.warehouse.Product;
import by.rozmysl.warehouse.Product1;
import by.rozmysl.warehouse.Product2;
import by.rozmysl.warehouse.Product3;

import java.util.ArrayList;

public class Waybill {
    private final long number;
    private final String operation;
    private final ArrayList<Product> products;

    public Waybill(long number, String operation, ArrayList<Product> products) {
        this.number = number;
        this.operation = operation;
        this.products = products;
    }

    public void addWaybill(String name, String address, int quantity) {
        if (address.charAt(0) == '1') products.add(new Product1(name, address, quantity));
        if (address.charAt(0) == '2') products.add(new Product2(name, address, quantity));
        if (address.charAt(0) == '3') products.add(new Product3(name, address, quantity));
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public String getOperation() {
        return operation;
    }

    public long getNumber() {
        return number;
    }
    @Override
    public String toString() {
        return "Waybill{" +
                ", products=" + products +
                '}';
    }
}
