package by.rozmysl.warehouse;

public interface Product {
    String getName();
    String getAddress();
    int getQuantity();

    void provideSpecialConditions(int turn);
}
