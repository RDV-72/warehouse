package by.rozmysl.warehouse;

public class Product2 implements Product {
    private final String name;
    private final String address;
    private final int quantity;

    public Product2(String name, String address, int quantity) {
        this.name = name;
        this.address = address;
        this.quantity = quantity;
    }

    @Override
    public void provideSpecialConditions(int turn) {
        if (turn == 1) System.out.println("Enable special system " + address.charAt(0));
        else System.out.println("Disabled special system " + address.charAt(0));
        p21();
        p22();
    }

    public void p21() {}
    public void p22() {}

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return name + " (quantity=" + quantity + ")";
    }
}