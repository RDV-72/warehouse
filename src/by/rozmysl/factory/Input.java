package by.rozmysl.factory;

import by.rozmysl.Waybill;
import by.rozmysl.warehouse.Product;

public class Input implements Warehouse {

    @Override
    public void makeOperations(Waybill waybill) {
        for (Product product : waybill.getProducts()) {
            System.out.println("put " + product.toString() + " in warehouse " +
                    product.getAddress().charAt(0) + " in cell " + product.getAddress());
            product.provideSpecialConditions(1);
        }
    }

    @Override
    public void reflectInAccounting(Waybill waybill) {
        for (Product product : waybill.getProducts()) {
            System.out.println(product.toString() + " added to the account.");
        }
    }

    @Override
    public String getResult(Waybill waybill) {
        return "According to waybill " + waybill.getNumber() + ", the product was accepted at the warehouse";
    }
}
