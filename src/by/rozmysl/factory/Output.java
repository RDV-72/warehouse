package by.rozmysl.factory;

import by.rozmysl.Waybill;
import by.rozmysl.warehouse.Product;

public class Output implements Warehouse {

    @Override
    public void makeOperations(Waybill waybill) {
        for (Product product : waybill.getProducts()) {
            System.out.println("take " + product.toString() + " from the warehouse " +
                    product.getAddress().charAt(0) + " from the cell " + product.getAddress());
            product.provideSpecialConditions(0);
        }
    }

    @Override
    public void reflectInAccounting(Waybill waybill) {
        for (Product product : waybill.getProducts()) {
            System.out.println(product.toString() + " taken away from the account.");
        }
    }

    @Override
    public String getResult(Waybill waybill) {
        return "According to waybill " + waybill.getNumber() + ", the product were shipped from the warehouse";
    }
}
