package by.rozmysl.factory;

import by.rozmysl.Waybill;

public interface Warehouse {
    void makeOperations(Waybill waybill);
    void reflectInAccounting(Waybill wayBill);
    String getResult(Waybill wayBill);
}

