package by.rozmysl.factory;

import by.rozmysl.Waybill;

public class Director {
    private final Warehouse warehouse;
    private final Waybill waybill;

    public Director(Warehouse warehouse, Waybill waybill) {
        this.warehouse = warehouse;
        this.waybill = waybill;
    }

    public String startWorking() {
        warehouse.makeOperations(waybill);
        warehouse.reflectInAccounting(waybill);
        return warehouse.getResult(waybill);
    }
}
