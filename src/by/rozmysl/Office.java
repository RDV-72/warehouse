package by.rozmysl;

import by.rozmysl.factory.Director;
import by.rozmysl.factory.Input;
import by.rozmysl.factory.Output;
import by.rozmysl.factory.Warehouse;

import java.util.ArrayList;

public class Office {

    public static void main(String[] args) {
        Waybill waybill1 = new Waybill(12345678L, "input", new ArrayList<>());
        waybill1.addWaybill("name 1", "145hfd45", 12);
        waybill1.addWaybill("name 2", "245wrh45", 60);
        waybill1.addWaybill("name 3", "312dgr14", 32);
        Waybill waybill2 = new Waybill(24567821L, "output", new ArrayList<>());
        waybill2.addWaybill("name 4", "178drt15", 12);
        waybill2.addWaybill("name 5", "265xcv45", 60);
        waybill2.addWaybill("name 6", "336ert29", 32);
        waybillCreationAndWarehouseLaunch(waybill1);
        System.out.println();
        waybillCreationAndWarehouseLaunch(waybill2);
    }

    public static void waybillCreationAndWarehouseLaunch(Waybill waybill) {
        Warehouse warehouse = null;
        if (waybill.getOperation().equals("input")) warehouse = new Input();
        else if (waybill.getOperation().equals("output")) warehouse = new Output();
        else System.out.println("Another warehouse operation.");

        Director director = new Director(warehouse, waybill);
        System.out.println(director.startWorking());
    }
}
